#!/bin/bash 

DUMP_USER="root"
DUMP_DB="wp_ewaa"
DUMP_TABLES="wp_shop_rota wp_shop_rota_volunteers"

#mysqldump -u root -p -c --skip-extended-insert -t wp_ewaa wp_shop_rota wp_shop_rota_volunteers
mysqldump -u ${DUMP_USER} -p -c --skip-extended-insert -t ${DUMP_DB} ${DUMP_TABLES}
