<?php

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
    <th><label for="contactnum"><?php _e("Contact Phone Num"); ?></label></th>
        <td>
            <input type="text" name="contactnum" id="contactnum" value="<?php echo esc_attr( get_the_author_meta( 'contactnum', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter the number, to contact you on in a hurry, regarding volunteering for the shop."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="actualname"><?php _e("Actual Namectualname"); ?></label></th>
        <td>
            <input type="text" name="actualname" id="actualname" value="<?php echo esc_attr( get_the_author_meta( 'actualname', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("The Actual name of this user."); ?></span>
        </td>
    </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'contactnum', $_POST['contactnum'] );
    update_user_meta( $user_id, 'actualname', $_POST['actualname'] );
}
