<?php

function shop_rota_capabilities_install () {
    // Gets the simple_role role object.
    $role = get_role( 'administrator' );
 
    // Add a new capability.
    $role->add_cap( 'shop_rota_import', true );
    $role->add_cap( 'shop_rota_export', true );
    $role->add_cap( 'shop_rota_edit_members', true );
    $role->add_cap( 'shop_rota_edit_rota', true );
}

function shop_rota_capabilities_uninstall () {
    // Gets the simple_role role object.
    $role = get_role( 'administrator' );
 
    // Add a new capability.
    $role->remove_cap( 'shop_rota_import', true );
    $role->remove_cap( 'shop_rota_export', true );
    $role->remove_cap( 'shop_rota_edit_members', true );
    $role->remove_cap( 'shop_rota_edit_rota', true );
}
