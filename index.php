<?php
/**
 * Plugin Name: Shop Rota Plugin
 * Plugin URI: http://ewaa.mediaserver/shop-rota-plugin
 * Description: A Plugin to present a shop rota in wordpress
 * Version: 1.0
 * Author: Simon Bazley
 * Author URI: http://www.mywebsite.com
 */

//add_action( 'the_content', 'my_thank_you_text' );

//function my_thank_you_text ( $content ) {
//    return $content .= '<p>Thank you for reading!</p>';
//}

require_once(dirname(__FILE__).'/shop-rota-block/index.php');
add_action( 'init', 'shop_rota_block_index');

require_once(dirname(__FILE__).'/profile-extension/index.php');
require_once(dirname(__FILE__).'/admin-page/index.php');
require_once(dirname(__FILE__).'/db.php');
require_once(dirname(__FILE__).'/capabilities.php');

register_activation_hook( __FILE__, 'shop_rota_db_install' );
add_action( 'plugins_loaded', 'shop_rota_update_db_check' );

register_activation_hook( __FILE__, 'shop_rota_capabilities_install' );
