<?php

function shop_rota_admin_shoprota_select_member($volunteers, $current_member=NULL)
{
    $options="";
    if ($current_member===NULL) {
        $options.="<option value=0 selected>Select Volunteer</option>\n";
    }
    foreach($volunteers as $index=>$text) {
        $options.="<option value=$index";
        if ($index == $current_member) {
            $options.=" selected";
        }
        $options.=">".$text."</option>";
    }
    return $options;
}

function shop_rota_admin_shoprota_release_options($current_state=0)
{
    $consents = array(0=>'Closed', 1=>'Available');
    $options="";
    foreach($consents as $index=>$text) {
        $options.="<option value=$index";
        if (((bool)$index) == $current_state) {
            $options.=" selected";
        }
        $options.=">".$text."</option>";
    }
    return $options;
}
function shop_rota_admin_shoprota_get_volunteers()
{
    global $wpdb,$current_user;
    $query_sql = 'SELECT id, name, keyholder FROM '.SR_SHOP_VOLUNTEERS;
    $volunteers=$wpdb->get_results($query_sql);
    return $volunteers;
}
function shop_rota_admin_shoprota_get_volunteers_index($volunteers, $keyholderstring='')
{
    $volunteers_index=array();
    foreach($volunteers as $value) {
        $volunteers_index[$value->id]="{$value->name}".((bool)$value->keyholder?$keyholderstring:"");
    }
    return $volunteers_index;
}
function shop_rota_admin_shoprota_get_keyholders($volunteers, $keyholder=true)
{
    $volunteers_index=array();
    foreach($volunteers as $value) {
        if (((bool)$value->keyholder) != ((bool)$keyholder)) {
           continue;
        }
        $volunteers_index[]=$value->id;
    }
    return $volunteers_index;
}
function shop_rota_admin_shoprota($from=NULL, $to=NULL)
{
    global $wpdb,$current_user;
    $out='';
    //$query_sql = 'SELECT id, name, keyholder FROM '.SR_SHOP_VOLUNTEERS;
    //$volunteers=$wpdb->get_results($query_sql);
    $volunteers=shop_rota_admin_shoprota_get_volunteers();
    //$volunteers_index=array();
    //foreach($volunteers as $value) {
    //    $volunteers_index[$value->id]="{$value->name}".((bool)$value->keyholder?" (has key)":"");
    //}
    $volunteers_index=shop_rota_admin_shoprota_get_volunteers_index($volunteers, ' (has key)');
    //var_dump($volunteers_index);
    if (isset($_POST['edit_id']) && wp_verify_nonce($_POST['shop-rota-block-nonce'], 'shop-rota-block-nonce')) {
        $edit_id=$_POST['edit_id'];
    }
    if (isset($_POST['save']) && wp_verify_nonce($_POST['shop-rota-block-nonce'], 'shop-rota-block-nonce')) {
        $save_id=$_POST['save'];
    }
    if (isset($_POST['delete']) && wp_verify_nonce($_POST['shop-rota-block-nonce'], 'shop-rota-block-nonce')) {
        if (isset($_POST['selected'])) {
            $selected = array_keys($_POST['selected']);
        } else {
            $selected = array();
        }
        shop_rota_admin_shoprota_delete_shoprota($selected);
    }

    if (!empty($save_id)) {
        $data_array=array_intersect_key($_POST, array_flip(array('keyholder','nonkeyholder','releasekeyholder','releasenonkeyholder')));
        $data_types=array_fill(0,count($data_array),'%d');

        $retval=$wpdb->update(
            SR_SHOP_ROTA,
            $data_array,
            array('id'=>$save_id),
            $data_types,
            '%d'
        );
        if (empty($retval)) {
            $error="Data not updated";
        } else {
            unset($_POST['save']);
        }
    }

    $query_sql = 'SELECT sr.id, sr.date, ksv.name AS ksv_name, ksv.id AS ksv_id, '.
                                'nksv.name AS nksv_name, nksv.id AS nksv_id, '.
                                    'sr.releasekeyholder, sr.releasenonkeyholder '.
    //                            ' FROM wp_shop_rota AS sr '.
                                ' FROM '.SR_SHOP_ROTA.' AS sr'.
                                    ' LEFT JOIN '.SR_SHOP_VOLUNTEERS.' AS ksv ON sr.keyholder = ksv.id'.
                                    ' LEFT JOIN '.SR_SHOP_VOLUNTEERS.' AS nksv ON sr.nonkeyholder = nksv.id';
    $range=array();
    if (!empty($from)) {
        $range[]="sr.date > '$from'";
    }
    if (!empty($to)) {
        $range[]="sr.date < '$to'";
    }
    $range_string = implode($range, ' AND ');
    if (!empty($range_string)) {
        $query_sql.= ' WHERE '.$range_string;
    }
    $query_sql.= ' ORDER BY sr.date';
    $results=$wpdb->get_results($query_sql);
    //var_dump($results);
    //$out.='Attributes: '.var_export($attributes, true)."<br>\n";
    //$out.='_POST: '.var_export($_POST, true)."<br>\n";
    //$out.=$query_sql;
    if (!empty($error)) {
        $out.="<p>Error: $error</p>\n";
    }

    $out.="<h2>Existing Rota Dates</h2>\n";

    $nonce = wp_create_nonce( 'shop-rota-block-nonce' );
    //$out.="<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
    $out.="<form action=\"\" method=\"post\">\n";
    $out.="<input type=hidden name=shop-rota-block-nonce value=\"${nonce}\">\n";

    $out.="<table border=1 cellspacing=0 cellpadding=2><tbody>";
    $out.="<tr>\n";
    if (empty($edit_id)) {
        $out.="<th></th>\n";
    }
    $out.="<th>Date</th>\n";
    $out.="<th>Keyholder</th>\n";
    $out.="<th>NonKeyHolder</th>\n";
    $out.="<th>KH Swap</th>\n";
    $out.="<th>NKH Swap</th>\n";
    $out.="</tr>\n";

    foreach($results AS $row) {
        $out.="<tr>\n";
        if (empty($edit_id) || $edit_id!=$row->id) {
            if (empty($edit_id)) {
                $selectedstring = (isset($selected) && in_array($row->id, $selected))?' checked':'';
                $out.="<td><input type=checkbox name=selected[$row->id] value=true$selectedstring></td>\n";
            }
            $out.="<td>".$row->date."</td>\n";
            $out.="<td>".$row->ksv_name."</td>\n";
            $out.="<td>".$row->nksv_name."</td>\n";
            $out.="<td>".($row->releasekeyholder?'Wants Swap':'No')."</td>\n";
            $out.="<td>".($row->releasenonkeyholder?'Wants Swap':'No')."</td>\n";
            if (empty($edit_id)) {
                $out.="<td><button type=submit name=edit_id class=\"button\" value=\"".$row->id."\">Edit</button></td>\n";
            }
        } else {
            $out.="<td>".$row->date."</td>\n";
            $out.="<td><select name=keyholder>\n".shop_rota_admin_shoprota_select_member($volunteers_index, $row->ksv_id)."</select></td>\n";
            $out.="<td><select name=nonkeyholder>\n".shop_rota_admin_shoprota_select_member($volunteers_index, $row->nksv_id)."</select></td>\n";
            $out.="<td><select name=releasekeyholder>\n".shop_rota_admin_shoprota_release_options($row->releasekeyholder)."</select></td>\n";
            $out.="<td><select name=releasenonkeyholder>\n".shop_rota_admin_shoprota_release_options($row->releasenonkeyholder)."</select></td>\n";
            $out.="<td><button type=submit name=save value={$row->id}>Save</button></td>\n";
            //$out.="<td><button type=reset name=cancel>Cancel</button></td>\n";
            $out.="<td><input type=\"button\" name=\"cancel\" value=\"Cancel\" ".
                    "onClick=\"window.location='{$_SERVER['PHP_SELF']}';\"/></td>\n";
        }
        $out.="</tr>\n";
    }
    $out.="</tbody></table>";
    if (empty($edit_id)) {
        $out.="<p class=submit>\n";
        $out.="<button type=submit class=\"button\" name=delete>Delete Selected</button>\n";
        $out.="</p>\n";
    }
    $out.="</form>\n";
    //echo esc_attr( get_the_author_meta( 'contactnum', $user->ID ) );
    //display_name
/*
*/
    return $out;
}

function shop_rota_admin_shoprota_enqueue_scripts_n_styles()
{
    // Load the datepicker script (pre-registered in WordPress).
    wp_enqueue_script( 'jquery-ui-datepicker' );

    // You need styling for the datepicker. For simplicity I've linked to the jQuery UI CSS on a CDN.
    wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );
}

function shop_rota_admin_shoprota_import_shoprota($shoprota)
{
    global $wpdb;
    foreach ($shoprota as $day) {
        if (is_array($day)) {
            $day = (object)$day;
        }
        if (!isset($day->unixdate)) {
            $errormessage = 'Not a shopduty day: '.var_export($day, true);
            error_log(__FILE__.':'.__LINE__.' '.$errormessage);
            throw(new Exception($errormessage));
            break;
        }

        $insert_array=
            array(
                'date' => date("c", $day->unixdate),
                'keyholder' => $day->keyholder,
                'nonkeyholder' => $day->nonkeyholder,
            );
        $result = $wpdb->get_results(
                'SELECT COUNT(ID) AS count'.
                ' FROM '.SR_SHOP_ROTA.
                ' WHERE date = \''.$insert_array['date'].'\'');
        $result = array_shift($result);
        if ($result->count == 0) {
            $wpdb->insert(
                SR_SHOP_ROTA,
                $insert_array
            );
        } else {
            $errormessage = 'Shop day already exists for '.$insert_array['date'];
            error_log(__FILE__.':'.__LINE__.$errormessage);
            throw(new Exception($errormessage));
            break;
        }
    }
}

function shop_rota_admin_shoprota_delete_shoprota($ids)
{
    global $wpdb;
    foreach ($ids as $id) {
        $wpdb->delete(
            SR_SHOP_ROTA,
            array( 'ID' => $id ),
            array( '%d' )
        );
    }
}

function shop_rota_admin_shoprota_newdates()
{
    $out='';
    if (isset($_POST['save-new-shop-rota']) && wp_verify_nonce($_POST['shop-rota-block-nonce'], 'shop-rota-block-nonce')) {
        $save_new_rota=$_POST['save-new-shop-rota'];
    }

    if (!empty($save_new_rota)) {
        $shoprotafrom=$_POST['shop-rota-from'];
        $shoprotato=$_POST['shop-rota-to'];
        $shoprotafromunix=strtotime($_POST['shop-rota-from']);
        $shoprotatounix=strtotime($_POST['shop-rota-to']);
        $shoprotakhoffset=(int)$_POST['shop-rota-khoffset'];
        $shoprotankhoffset=(int)$_POST['shop-rota-nkhoffset'];
    } else {
        $shoprotafrom=get_option('shop-rota-admin-shoprotafrom', '');
        $shoprotato=get_option('shop-rota-admin-shoprotato', '');
        $shoprotakhoffset=get_option('shop-rota-admin-shoprotakhoffset', 0);
        $shoprotankhoffset=get_option('shop-rota-admin-shoprotankhoffset', 0);
    }
    $startunixdate=strtotime($shoprotafrom);
    $endunixdate=strtotime($shoprotato);
    $duration=($endunixdate - $startunixdate)/604800; // Number of seconds in a week
    if ($duration < 0) { $duration = 1; }
    $volunteers = shop_rota_admin_shoprota_get_volunteers();
    $keyholders = shop_rota_admin_shoprota_get_keyholders($volunteers, true);
    $nonkeyholders = shop_rota_admin_shoprota_get_keyholders($volunteers, false);
    $nkeyholders = count($keyholders);
    $nnonkeyholders = count($nonkeyholders);
    $shoprota = array();
    if ($nkeyholders>0 && $nnonkeyholders>0) {
        for ($week=0;$week<$duration;$week++) {
            $keyholder = $keyholders[($week+$shoprotakhoffset)%$nkeyholders];
            $nonkeyholder = $nonkeyholders[($week+$shoprotankhoffset)%$nnonkeyholders];
            $unixtime = strtotime("+".$week."week", $startunixdate);
            //$fulldate = strftime("%Y %B %d", $unixtime);
            $fulldate = strftime("%Y-%m-%d", $unixtime);
            $month = strftime("%m", $unixtime);
            if ($month==8) {
                continue;
            }
            $shoprota[] = (object)array('date'=>$fulldate, 'unixdate'=>$unixtime, 'keyholder'=>$keyholder, 'nonkeyholder'=>$nonkeyholder);
        }
        $nextshoprotafrom = date("d-m-Y", $startunixdate+($week*604800));
        $nextshoprotato = date("d-m-Y", $startunixdate+(($week+$duration)*604800));
        $nextshoprotakhoffset = (($week+$shoprotakhoffset)%$nkeyholders);
        $nextshoprotankhoffset = (($week+$shoprotankhoffset)%$nnonkeyholders);
    }

    $out.="<h2>Generate new rota dates</h2\n";
    $out.="<form action=\"\" method=\"post\">\n";

    if (!empty($save_new_rota)) {
        if ($save_new_rota=='preview' || $save_new_rota=='save') {
            $volunteers_index=shop_rota_admin_shoprota_get_volunteers_index($volunteers);
            //$out.="<form action=\"\" method=\"post\">\n";
            $out.="<table border=1 cellspacing=0 cellpadding=2><tbody>";
            $out.="<tr>\n";
            $out.="<th>Date</th>\n";
            $out.="<th>Keyholder</th>\n";
            $out.="<th>NonKeyHolder</th>\n";
            $out.="</tr>\n";
            foreach($shoprota AS $row) {
                $out.="<tr>\n";
                $out.="<td>".$row->date."</td>\n";
                $out.="<td>".$volunteers_index[$row->keyholder]."</td>\n";
                $out.="<td>".$volunteers_index[$row->nonkeyholder]."</td>\n";
                $out.="</tr>\n";
            }
            $out.="</tbody></table>";
            if ($save_new_rota=='preview') {
                $out.="<button type=submit name=save-new-shop-rota class=\"button button-primary\" value=save>Save New Rota</button>\n";
            }
        }
        if ($save_new_rota=='save') {
            try {
                shop_rota_admin_shoprota_import_shoprota($shoprota);
                update_option('shop-rota-admin-shoprotafrom', $nextshoprotafrom);
                update_option('shop-rota-admin-shoprotato', $nextshoprotato);
                update_option('shop-rota-admin-shoprotakhoffset', $nextshoprotakhoffset);
                update_option('shop-rota-admin-shoprotankhoffset', $nextshoprotankhoffset);
            } catch (Exception $e) {
                $errormessage=$e->getMessage();
                $out.="<p>ErrorMessage: $errormessage</p>\n";
            }
        } else if ($save_new_rota=='update') {
            update_option('shop-rota-admin-shoprotafrom', $shoprotafrom);
            update_option('shop-rota-admin-shoprotato', $shoprotato);
            update_option('shop-rota-admin-shoprotakhoffset', $shoprotakhoffset);
            update_option('shop-rota-admin-shoprotankhoffset', $shoprotankhoffset);
        }
    }

    $nonce = wp_create_nonce( 'shop-rota-block-nonce' );
    //$out.="<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
    $out.="<input type=hidden name=shop-rota-block-nonce value=\"${nonce}\">\n";
    $out.="<table class=\"form-table\" role=\"presentation\">\n<tbody>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Start From Date</label></th>\n";
    $out.="<td><input type=text id=shop-rota-from name=shop-rota-from value=\"${shoprotafrom}\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Generate To Date</label></th>\n";
    $out.="<td><input type=text id=shop-rota-to name=shop-rota-to value=\"${shoprotato}\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Keyholder Start Offset</label></th>\n";
    $out.="<td><input type=text id=shop-rota-khoffset name=shop-rota-khoffset value=\"${shoprotakhoffset}\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Non-Keyholder Start Offset</label></th>\n";
    $out.="<td><input type=text id=shop-rota-nkhoffset name=shop-rota-nkhoffset value=\"${shoprotankhoffset}\"></td>\n";
    $out.="</tr>\n";
    $out.="</tbody></table>\n<p class=submit>\n";
    $buttonclass="button";
    if (empty($save_new_rota) || $save_new_rota!='preview') {
        $buttonclass.=" button-primary";
    }
    $out.="<button type=submit name=save-new-shop-rota class=\"$buttonclass\" value=preview>Preview New Rota</button>\n";
    $out.="<button type=submit name=save-new-shop-rota class=\"button\" value=update>Update Default dates/offsets</button>\n";
    $out.="</p>\n";
    $out.="</form>\n";

    $out.="<script>\n";
    $out.="  jQuery(document).ready(function($) {\n";
    $out.="    jQuery(\"#shop-rota-from\").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat : \"dd-mm-yy\",
      showWeek: true,
      firstDay: 1,
      numberOfMonths: 3,
      beforeShowDay: function(date) {
        var day = date.getDay();
        return [(day == 0)];
      }
    });\n";
    $out.="    jQuery(\"#shop-rota-to\").datepicker({
      changeMonth: true,
      changeYear: true,
      showWeek: true,
      dateFormat : \"dd-mm-yy\",
      firstDay: 1,
      numberOfMonths: 3,
      beforeShowDay: function(date) {
        var day = date.getDay();
        return [(day == 0)];
      }
    });\n";
    $out.="  });\n";
    $out.="</script>\n";
    return $out;
}
