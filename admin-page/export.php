<?php
require_once(plugin_dir_path(dirname(__FILE__)).'/db.php');

function shop_rota_admin_export_tab($form_action) {
        ob_start();
?>
		<form action="<?php echo esc_url( $form_action ) ?>" method="post">
        <p class=submit>
            <button type="submit" name="export_json" class="button" value="export">Download Shop Rota data export file</button>
        </p>
        </form>
<?php
        return ob_get_clean();
}

function shop_rota_admin_export_get_rota($from=NULL, $to=NULL) {
    global $wpdb;
	$query_sql = 'SELECT * FROM '.SR_SHOP_ROTA;
    if ($from !== NULL || $to !== NULL) {
        $query_sql.=' WHERE ';
    }
    if ($from !== NULL) {
        $query_sql.='date > \''.$from.'\'';
    }
    if ($from !== NULL && $to !== NULL) {
        $query_sql.=' AND ';
    }
    if ($to !== NULL) {
        $query_sql.='date < \''.$to.'\'';
    }
	$results=$wpdb->get_results($query_sql);
    //var_dump($query_sql);
    //var_dump($query_sql, $results);
    //echo json_encode($results, JSON_PRETTY_PRINT);
    return $results;
    //exit;
}

function shop_rota_admin_export_get_volunteers() {
    global $wpdb;
	$query_sql = 'SELECT * FROM '.SR_SHOP_VOLUNTEERS.';';
	$results=$wpdb->get_results($query_sql);
    //var_dump($query_sql, $results);
    //echo json_encode($results, JSON_PRETTY_PRINT);
    return $results;
    //exit;
}

function shop_rota_admin_export_members() {
  $filename='export_data.json';
  $data=array('members'=>array('name'=>'Simon Bazley'));
  shop_rota_admin_export_json_file($data, $filename);
}

function shop_rota_admin_export_json_file($data, $filename='export_data.json') {
  //if (!current_user_can('shop_rota_export')) {
  //   echo "You do not have permission to download this file";
  //   exit();
  //}
  //$json = json_encode($data, JSON_PRETTY_PRINT);
  $content_type='text/plain';

  header("Expires: 0");
  header("Cache-Control: no-cache, no-store, must-revalidate"); 
  header('Cache-Control: pre-check=0, post-check=0, max-age=0', false); 
  header("Pragma: no-cache");   
  header("Content-type: {$content_type}");
  header("Content-Disposition:attachment; filename={$filename}");
  header("Content-Type: application/force-download");
   
  echo json_encode($data, JSON_PRETTY_PRINT);
  
  exit();
}

function shop_rota_admin_check_exports() {
    if (isset($_GET['page']) && $_GET['page'] === 'shop-rota-settings' && isset($_POST['export_json'])) {// &&
        //wp_verify_nonce($_POST['shop-rota-block-nonce-import-export'], 'shop-rota-block-nonce-import-export')) {
        switch ($_POST['export_json']) {
            case 'export':
            default:
                $data = array(
                    'members'=>shop_rota_admin_export_get_volunteers(),
                    'shopduty'=>shop_rota_admin_export_get_rota(),
                );
                shop_rota_admin_export_json_file($data);
                //shop_rota_admin_export_members();
            break;
        }
    }
}
