<?php
function shop_rota_admin_members_delete_members($ids)
{
    global $wpdb;
    foreach ($ids as $id) {
        $wpdb->delete(
            SR_SHOP_VOLUNTEERS,
            array( 'ID' => $id ),
            array( '%d' )
        );
    }
}

function shop_rota_admin_members()
{
	global $wpdb; //,$current_user;
    $edit_id=$save_id=$selected=$create=NULL;
    $out='';

    //var_dump($_POST);

    if (isset($_POST['edit_id']) && wp_verify_nonce($_POST['shop-rota-block-nonce-members'], 'shop-rota-block-nonce-members')) {
        $edit_id=$_POST['edit_id'];
    }
    if (isset($_POST['save']) && wp_verify_nonce($_POST['shop-rota-block-nonce-members'], 'shop-rota-block-nonce-members')) {
        $save_id=$_POST['save'];
    }
    if (isset($_POST['delete']) && wp_verify_nonce($_POST['shop-rota-block-nonce-members'], 'shop-rota-block-nonce-members')) {
        if (isset($_POST['selected'])) {
            $selected = array_keys($_POST['selected']);
        } else {
            $selected = array();
        }
        shop_rota_admin_members_delete_members($selected);
    }
    if (isset($_POST['create']) && wp_verify_nonce($_POST['shop-rota-block-nonce-members'], 'shop-rota-block-nonce-members')) {
        $create=true;
    }
    //var_dump($edit_id, $save_id, $selected, $create);

    if (!empty($save_id)) {
        $data_array=array_intersect_key($_POST, array_flip(array('uid','name','email','phone','lastconsent','keyholder','keycode')));
        $data_types=array('uid'=>'%d', 'name'=>'%s', 'email'=>'%s', 'phone'=>'%s', 'lastconsent'=>'%s', 'keyholder'=>'%d', 'keycode'=>'%s');

        if ($data_array['lastconsent']==0) {
            unset($data_array['lastconsent']);
            unset($data_types['lastconsent']);
        } else {
            $data_array['lastconsent']=current_time('mysql', 1);
        }
        $retval=$wpdb->update(
            SR_SHOP_VOLUNTEERS,
            $data_array,
            array('id'=>$save_id),
            $data_types,
            '%d'
        );
        if (empty($retval)) {
            $error="Data not updated";
        } else {
            unset($_POST['save']);
        }
    }

    if (!empty($create) && $create==true) {
        $newmember = shop_rota_admin_new_member();
        if ($newmember!==true) {
            return $newmember;
        }
    }

    $query_sql = 'SELECT * FROM '.SR_SHOP_VOLUNTEERS;
	$results=$wpdb->get_results($query_sql);

    $out.="<h2>Existing Shop Rota Members</h2>\n";

    $nonce = wp_create_nonce( 'shop-rota-block-nonce-members' );
    //$out.="<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
    $out.="<form action=\"\" method=\"post\">\n";
    $out.="<input type=hidden name=shop-rota-block-nonce-members value=\"${nonce}\">\n";
    $out.="<table border=1 cellspacing=0 cellpadding=2><tbody>";
    $out.="<tr>\n";
    $out.="<th></th>\n";
    $out.="<th>ID</th>\n";
    $out.="<th>UID</th>\n";
    $out.="<th>Name</th>\n";
    $out.="<th>Email</th>\n";
    $out.="<th>Phone</th>\n";
    $out.="<th>Consent</th>\n";
    $out.="<th>KeyHolder</th>\n";
    $out.="<th>KeyCode</th>\n";
    $out.="</tr>\n";

	foreach($results AS $row) {
        $out.="<tr>\n";
        $hasconsent=!(empty($row->lastconsent) || $row->lastconsent=='0000-00-00 00:00:00');
        $iskeyholder=(bool)$row->keyholder;
        if (empty($edit_id) || $edit_id!=$row->id) {
            if (empty($edit_id)) {
                $selectedstring = (isset($selected) && in_array($row->id, $selected))?' checked':'';
                $out.="<td><input type=checkbox name=selected[$row->id] value=true$selectedstring></td>\n";
            }
            $out.="<td>".$row->id."</td>\n";
            $out.="<td>".$row->uid."</td>\n";
            $out.="<td>".$row->name."</td>\n";
            $out.="<td>".$row->email."</td>\n";
            $out.="<td>".$row->phone."</td>\n";
            $out.="<td>".($hasconsent?$row->lastconsent:'Consent Never Given')."</td>\n";
            $out.="<td>".$row->keyholder."</td>\n";
            $out.="<td>".$row->keycode."</td>\n";
            if (empty($edit_id)) {
                $out.="<td><button type=submit name=edit_id class=\"button\" value=\"".$row->id."\">Edit</button></td>\n";
            }
        } else {
            $out.="<td>".$row->id."</td>\n";
            $out.="<td><input type=text name=uid size=4 value=\"".$row->uid."\"></td>\n";
            $out.="<td><input type=text name=name value=\"".$row->name."\"></td>\n";
            $out.="<td><input type=text name=email size=32 value=\"".$row->email."\"></td>\n";
            $out.="<td><input type=text name=phone size=12 value=\"".$row->phone."\"></td>\n";
            $consents = array();
            $options='';
            if (!$hasconsent) {
                $consents[] = 'No Consent';
                $consents[] = 'Give Consent';
            } else {
                //$consents[] = 'Consent given '.$row->lastconsent;
                $consents[] = 'Consent given ';
                $consents[] = 'Update Consent';
            }
            foreach($consents as $index=>$text) {
                $options.="<option value=$index";
                if ($index==0) {
                    $options.=" selected";
                }
                $options.=">".$text."</option>";
            }
            $out.="<td><select name=lastconsent>\n".$options."</select></td>\n";
            $consents = array(0=>'Not Keyholder', 1=>'Is Keyholder');
            $options='';
            foreach($consents as $index=>$text) {
                $options.="<option value=$index";
                if (((bool)$index) == $iskeyholder) {
                    $options.=" selected";
                }
                $options.=">".$text."</option>";
            }
            $out.="<td><select name=keyholder>\n".$options."</select></td>\n";
            $out.="<td><input type=text name=keycode size=4 value=\"".$row->keycode."\"></td>\n";
            $out.="<td><button type=submit name=save value={$row->id}>Save</button></td>\n";
            //$out.="<td><button type=reset name=cancel>Cancel</button></td>\n";
            $out.="<td><input type=\"button\" name=\"cancel\" value=\"Cancel\" ".
                    //"onClick=\"window.location='{$_SERVER['PHP_SELF']}';\"/></td>\n";
                    "onClick=\"window.location='';\"/></td>\n";

        }
        $out.="</tr>\n";
    }
    $out.="</tbody></table>";
    if (empty($edit_id)) {
        $out.="<p class=submit>\n";
        $out.="<button type=submit class=\"button\" name=delete>Delete Selected</button>\n";
        $out.="<button type=submit class=\"button button-primary\" name=create>New Member</button>\n";
        $out.="</p>\n";
    }
    $out.="</form>\n";
    return $out;
}

function shop_rota_admin_new_member()
{
    global $wpdb;
    $out='';
    $error='';
    $default_member = array('uid'=>'', 'name'=>'', 'email'=>'', 'phone'=>'', 'hasconsent'=>false, 'keyholder'=>0, 'keycode'=>'');

    if (isset($_POST['save-new-member'])) {// && wp_verify_nonce($_POST['shop-rota-block-nonce-members'], 'shop-rota-block-nonce-members')) {
        $save_new=true;
    } else {
        $save_new=false;
    }

    //var_dump($_POST, $save_new);

    if (!empty($save_new)) {
        $data_array=array_intersect_key($_POST, array_flip(array('uid','name','email','phone','lastconsent','keyholder','keycode')));
        $data_types=array('uid'=>'%d', 'name'=>'%s', 'email'=>'%s', 'phone'=>'%s', 'lastconsent'=>'%s', 'keyholder'=>'%d', 'keycode'=>'%s');

        if ($data_array['lastconsent']==0) {
            unset($data_array['lastconsent']);
            unset($data_types['lastconsent']);
        } else {
            $data_array['lastconsent']=current_time('mysql', 1);
        }
        $retval=$wpdb->insert(
            SR_SHOP_VOLUNTEERS,
            $data_array,
            $data_types
        );
        if (empty($retval)) {
            $error="Data not updated";
            $row = (object) $data_array;
        } else {
            unset($_POST['create']);
            return true;
        }
    }
    //var_dump($row);
    if (!isset($row)) {
        $row = (object) $default_member;
    }
    $hasconsent=!(empty($row->lastconsent) || $row->lastconsent=='0000-00-00 00:00:00');
    $iskeyholder=(bool)$row->keyholder;
    $nonce = wp_create_nonce( 'shop-rota-block-nonce-members' );
    $out.="<form action=\"\" method=\"post\">\n";
    $out.="<input type=hidden name=shop-rota-block-nonce-members value=\"${nonce}\">\n";
    $out.="<input type=hidden name=create value=\"\">\n";
    $out.="<p><div class=error>\n".$error."\n</div></p>\n";
    $out.="<table class=\"form-table\" role=\"presentation\">\n<tbody>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>UID</label></th>\n";
    $out.="<td><input type=text id=shop-rota-new-member-uid name=uid value=\"".$row->uid."\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Name</label></th>\n";
    $out.="<td><input type=text id=shop-rota-new-member-name name=name value=\"".$row->name."\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Email</label></th>\n";
    $out.="<td><input type=text id=shop-rota-new-member-email name=email size=32 value=\"".$row->email."\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Phone</label></th>\n";
    $out.="<td><input type=text id=shop-rota-new-member-phone name=phone size=12 value=\"".$row->phone."\"></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>Consent</label></th>\n";
            $consents = array();
            $options='';
            if (!$hasconsent) {
                $consents[] = 'No Consent';
                $consents[] = 'Give Consent';
            } else {
                //$consents[] = 'Consent given '.$row->lastconsent;
                $consents[] = 'Consent given ';
                $consents[] = 'Update Consent';
            }
            foreach($consents as $index=>$text) {
                $options.="<option value=$index";
                if ($index==0) {
                    $options.=" selected";
                }
                $options.=">".$text."</option>";
            }
    $out.="<td><select id=shop-rota-new-member-lastconsent name=lastconsent>\n".$options."</select></td>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>KeyHolder</label></th>\n";
            $consents = array(0=>'Not Keyholder', 1=>'Is Keyholder');
            $options='';
            foreach($consents as $index=>$text) {
                $options.="<option value=$index";
                if (((bool)$index) == $iskeyholder) {
                    $options.=" selected";
                }
                $options.=">".$text."</option>";
            }
    $out.="<td><select id=shop-rota-new-member-keyholder name=keyholder>\n".$options."</select></td>\n";
    $out.="</tr>\n";
    $out.="<tr>\n<th scope=\"row\">\n<label>KeyCode</label></th>\n";
    $out.="<td><input type=text id=shop-rota-new-member-keycode name=keycode size=4 value=\"{$row->keycode}\"></td>\n";
    $out.="</tr>\n";
    $out.="</tbody></table>\n<p class=submit>\n";
    $out.="<button type=submit name=save-new-member class=\"button button-primary\" value=create-new>Save new member</button>\n";
    $out.="</p>\n";
    $out.="</form>\n";
    return $out;
}
