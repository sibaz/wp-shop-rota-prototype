<?php
require_once(plugin_dir_path(dirname(__FILE__)).'/db.php');

function shop_rota_admin_import_tab($form_action) {
        if (isset($_POST['import_json']) && wp_verify_nonce($_POST['shop-rota-block-nonce-import-export'], 'shop-rota-block-nonce-import-export')) {
            $mime_type = mime_content_type($_FILES["fileToUpload"]["tmp_name"]);
            if ($mime_type!="text/plain") {
                $error = "Unexpected mime-type '$mime_type' for json file (expected text/plain)";
            } else {
                $file_contents=file_get_contents($_FILES["fileToUpload"]["tmp_name"]);
                $file_json = json_decode($file_contents, true);
                $error = shop_rota_admin_import_json($file_json);
            }
        }
        $nonce = wp_create_nonce( 'shop-rota-block-nonce-import-export' );
        ob_start();
 ?>
        <form action="<?php echo esc_url( $form_action ) ?>" method="post" enctype="multipart/form-data">
        <input type=hidden name=shop-rota-block-nonce-import-export value="<?php echo $nonce ?>">
<?php
        if (!empty($error)) { ?>
        <p><div class=error><?php echo $error?></div></p><?php
        }
?>
        <table class="form-table" role="presentation">
            <tbody>
            <tr>
                <th scope="row">
                    <label>
            Select json file to import data from:
                    </label>
                </th>
                <td>
            <input type="file" name="fileToUpload" id="fileToUpload">
                </td>
            <tr>
            </tbody>
        </table>
        <p class=submit>
            <button type="submit" name="import_json" class="button">Import Shop Rota data from json export file</button>
        </p>
        </form>
<?php
        return ob_get_clean();
}

function shop_rota_admin_import_json($json) {
    if (!current_user_can('shop_rota_import')) {
        echo "You do not have permission to download this file";
        exit();
    }
    global $wpdb;

    if (isset($json['members'])) {
        $members = $json['members'];
    }
    if (isset($json['shopduty'])) {
        $shopduty = $json['shopduty'];
    }

    //var_dump($members, $shopduty);
    $errors=array();
/*

        {
            "date": "2020 August 02",
            "unixdate": 1596326400,
            "keyholder": 100,
            "nonkeyholder": 114
        }
        date date DEFAULT '0000-00-00' NOT NULL,
        keyholder mediumint(9) NOT NULL,
        nonkeyholder mediumint(9) NOT NULL,
    ]
}sbazley@simonspc:/volume1/ewaa-test-data$ ls
data.json  empty  members.json  positions.json  rotamembers.json
sbazley@simonspc:/volume1/ewaa-test-data$ cat members.json 
{
    "100": {
        "name": "Chris Clifford",
        "uid": 100,
        "email": "chrisclifford1969@yahoo.com",
                "phone": "07764 198046",
        "shopvolunteer": true,
                "consent":      true,
        "keyholder": true
    },

        name tinytext NOT NULL,
        email text DEFAULT ''NOT NULL,
        phone text DEFAULT '' NOT NULL,
        lastconsent datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        keyholder boolean DEFAULT FALSE NOT NULL,
        keycode tinytext DEFAULT '' NOT NULL,

*/
    $members_index=array();
    foreach ($members as $index=>$member) {
        if (!isset($member['email'])) {
            error_log(__FILE__.':'.__LINE__.' Not a member: '.var_export($member, true));
            $errors[]=' Not a member: '.var_export($member, true);
            break;
        }

        if (isset($member['shopvolunteer']) && !$member['shopvolunteer']) {
            continue;
        }

        $insert_array=
            array( 
                'name' => $member['name'],
                'uid' => $member['id'],
                'email' => $member['email'],
                'phone' => $member['phone'],
                'keyholder' => $member['keyholder']
            );
        if (isset($member['lastconsent'])) {
            $member['consent'] = $member['lastconsent'];
        }
        if (isset($member['consent'])) {
            $date = strtotime($member['consent']);
            if ($date) {
                $insert_array['lastconsent'] = date("c", $date);
            } else {
                $insert_array['lastconsent'] = current_time( 'mysql' );
            }
        }
        $retval = $wpdb->insert( 
            SR_SHOP_VOLUNTEERS, 
            $insert_array
        );
        if ($retval) {
            $members_index[$member['id']]=$wpdb->insert_id;
        }
    }
    foreach ($shopduty as $day) {
        if (isset($day['date'])) {
            $day['unixdate'] = strtotime($day['date']);
        }
        if (!isset($day['unixdate'])) {
            error_log(__FILE__.':'.__LINE__.' Not a shopduty day: '.var_export($day, true));
            $errors[]=' Not a shopduty day: '.var_export($day, true);
            break;
        }

        $insert_array=
            array( 
                'date' => date("c", $day['unixdate']),
                'keyholder' => $members_index[$day['keyholder']],
                'nonkeyholder' => $members_index[$day['nonkeyholder']],
            );
        //var_dump($insert_array);
        $wpdb->insert( 
            SR_SHOP_ROTA, 
            $insert_array
        );
    }
    if (!empty($errors)) {
        var_dump($errors);
        $error = '<p>'.implode('</p><p>', $errors).'</p>';
        return $error;
    }
}
