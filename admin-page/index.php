<?php
require_once(dirname(__FILE__).'/import.php');
require_once(dirname(__FILE__).'/export.php');
require_once(dirname(__FILE__).'/members.php');
require_once(dirname(__FILE__).'/shoprota.php');

add_action('admin_menu', 'shop_rota_admin_page_menu' );
add_action('admin_enqueue_scripts', 'shop_rota_admin_shoprota_enqueue_scripts_n_styles' );
shop_rota_admin_check_exports();

//add_filter('template_include', dirname(__FILE__).'/
//        array( $this, 'view_project_template') 
//    );

function shop_rota_admin_page_menu() {
	add_options_page( 'Shop Rota Settings', 'Shop Rota', 'manage_options', 'shop-rota-settings', 'shop_rota_admin_settings_page' );
}

function shop_rota_admin_settings_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	// We're saving our own options, until the WP Settings API is updated to work with Multisite.
	$form_action = add_query_arg( 'page', 'shop-rota-settings', bp_get_admin_url( 'admin.php' ) );
    $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'importexport';
    if ($active_tab=='importexport' && !current_user_can('shop_rota_import') &&!current_user_can('shop_rota_export')) {
        $active_tab = 'no_permission';
    }
    if ($active_tab=='members' && !current_user_can('shop_rota_edit_members')) {
        $active_tab = 'no_permission';
    }
    if ($active_tab=='shoprota' && !current_user_can('shop_rota_edit_rota')) {
        $active_tab = 'no_permission';
    }
    //wp_enqueue_style('table-styles_css', dirname(dirname(__FILE__)).'/table-styles.css');
    wp_enqueue_style('shop-rota-table-styles-css');
	?>

   	<div class="wrap">
		<h1>Shop Rota Settings</h1>
		<h2 class="nav-tab-wrapper"><?php shop_rota_admin_tabs($active_tab); ?></h2>
<?php 
    if ($active_tab=='no_permission') {
?>
        <h2>Access Denied</h2>
        <p>You do not have sufficient permission to view the admin pages</p>
<?php
    } else if ($active_tab=='importexport') {
        echo shop_rota_admin_import_tab($form_action);
        echo shop_rota_admin_export_tab($form_action);
    } else if ($active_tab=='members') {
        echo shop_rota_admin_members();
    } else if ($active_tab=='shoprota') {
?>
        <div>
<?php        echo shop_rota_admin_shoprota(); ?>
        </div><div>
<?php        echo shop_rota_admin_shoprota_newdates(); ?>
        </div>
<?php
    }
?>
	</div>

<?php
}

function shop_rota_admin_tabs( $active_tab = '' ) {
	$tabs_html    = '';
	$idle_class   = 'nav-tab';
	$active_class = 'nav-tab nav-tab-active';

    $base_url = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
    if (!empty($_SERVER['QUERY_STRING'])) {
        $base_url.='&tab=';
    }

	$tabs         = array(array('name'=>'Import/Export', 'href'=>$base_url.'importexport', 'slug'=>'importexport'), 
                          array('name'=>'Review Members',   'href'=>$base_url.'members', 'slug'=>'members'),
                          array('name'=>'Review Shop Rota',   'href'=>$base_url.'shoprota', 'slug'=>'shoprota'),
                        );

	// Loop through tabs and build navigation.
	foreach ( array_values( $tabs ) as $tab_data ) {
		$is_current = (bool) ( $tab_data['name'] == $active_tab || $active_tab==$tab_data['slug']);
		$tab_class  = $is_current ? $active_class : $idle_class;
		$tabs_html .= '<a href="' . esc_url( $tab_data['href'] ) . '" class="' . esc_attr( $tab_class ) . '">' . esc_html( $tab_data['name'] ) . '</a>';
	}

	echo $tabs_html;
}
?>
