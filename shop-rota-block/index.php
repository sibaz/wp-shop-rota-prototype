<?php

function shop_rota_block_shop_rota( $attributes ) {
	//error_log(__FILE__.':'.__LINE__.' '.var_export($attributes, true));
    require_once(dirname(__FILE__) .'/block.php');
    return shop_rota_shop_rota(NULL,NULL,$attributes);
	//return "<h3>SomeTestText</h3>\n";
}

function shop_rota_block_index() {
    /***********************************************************************************************
    *
    * Sessions
    *
    ***********************************************************************************************/
    wp_register_style(
	    'shop-rota-table-styles-css',
	    plugins_url( 'table-styles.css', dirname(__FILE__) )
    );
	wp_register_script(
		'shop-rota-shop-rota-block-block',
		plugins_url( 'block.js', __FILE__ ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-editor' )
	);
    register_block_type( 'shop-rota/shop-rota-block', array(
		'attributes'    => array(
            'range_from'    => array('type'=>'string', 'default'=>NULL),
            'range_to'      => array('type'=>'string', 'default'=>NULL),
        ),
        'editor_script'   => 'shop-rota-shop-rota-block-block', // The script name we gave in the wp_register_script() call.
        'render_callback' => 'shop_rota_block_shop_rota',
    ) );
}

