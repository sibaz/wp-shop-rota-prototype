<?php
/*
2016-09-21 added debug info
*/

function shop_rota_shop_rota($what=NULL,$what_id=NULL,$attributes)
{
	//error_log(__FILE__.':'.__LINE__.' '.var_export($what, true));
	//error_log(__FILE__.':'.__LINE__.' '.var_export($what_id, true));
	global $wpdb,$current_user;
    $out='';

    $attributes['show_title']=true;
    $attributes['show_year']=false;
    $attributes['show_brev_mdate']=true;

	wp_get_current_user();

	$query_sql = 'SELECT id, uid, keyholder FROM '.SR_SHOP_VOLUNTEERS.' WHERE email=\''.$current_user->user_email.'\'';
	$results=$wpdb->get_results($query_sql);
    if (!empty($results)) {
        //$out.='current_results: '.var_export($results, true)."\n";
        $result = array_shift($results);
        $current_id=$result->id;
        $current_keyholder=$result->keyholder;
    } else {
        $current_id = NULL;
        $current_keyholder= NULL;
    }

    if ($current_id !== NULL) {
        $update_data_array = NULL;
        $update_where_id = NULL;
        $nonce=(isset($_POST['shop-rota-block-nonce'])?$_POST['shop-rota-block-nonce']:'');
        if (wp_verify_nonce($nonce, 'shop-rota-block-nonce')) {
            if (!empty($_POST['ReleaseKey'])) {
                $change_key='ReleaseKey';
                $update_data_array = array('releasekeyholder'=>1);
                $update_where_id = $_POST[$change_key];
            } else if (!empty($_POST['ReconfirmKey'])) {
                $change_key='ReconfirmKey';
                $update_data_array = array('releasekeyholder'=>0);
                $update_where_id = $_POST[$change_key];
            } else if (!empty($_POST['AdoptKey'])) {
                $change_key='AdoptKey';
                $update_data_array = array('releasekeyholder'=>0, 'keyholder'=>$current_id);
                $update_where_id = $_POST[$change_key];
            } else if (!empty($_POST['ReleaseNKey'])) {
                $change_key='ReleaseNKey';
                $update_data_array = array('releasenonkeyholder'=>1);
                $update_where_id = $_POST[$change_key];
            } else if (!empty($_POST['ReconfirmNKey'])) {
                $change_key='ReconfirmNKey';
                $update_data_array = array('releasenonkeyholder'=>0);
                $update_where_id = $_POST[$change_key];
            } else if (!empty($_POST['AdoptNKey'])) {
                $change_key='AdoptNKey';
                $update_data_array = array('releasenonkeyholder'=>0, 'nonkeyholder'=>$current_id);
                $update_where_id = $_POST[$change_key];
            }
                //$out.='update_data_array: '.var_export($update_data_array, true);
                //$out.='update_where_id: '.var_export($update_where_id, true);
                //$out.='update_formats_array: '.var_export($update_formats_array, true);
            if (!empty($update_data_array) && !empty($update_where_id)) {
                $update_formats_array=array_fill(0,count($update_data_array),'%d');
                //$out.='update_data_array: '.var_export($update_data_array, true);
                //$out.='update_where_id: '.var_export($update_where_id, true);
                //$out.='update_formats_array: '.var_export($update_formats_array, true);
                $retval=$wpdb->update(
                    SR_SHOP_ROTA,
                    $update_data_array,
                    array('id'=>$update_where_id),
                    $update_formats_array,
                    '%d'
                );
                if (empty($retval)) {
                    $error="Data not updated";
                } else {
                    unset($_POST[$change_key]);
                }
            }
        }
    }

    //$out.='current_keyholder: '.var_export($current_keyholder, true)."\n";
    //$out.='current_uid: '.var_export($current_uid, true)."\n";
	$query_sql = 'SELECT sr.id, sr.date, ksv.name AS ksv_name, ksv.id AS ksv_id, '.
                                'nksv.name AS nksv_name, nksv.id AS nksv_id, '.
                                    'sr.releasekeyholder, sr.releasenonkeyholder '.
    //                            ' FROM wp_shop_rota AS sr '.
                                ' FROM '.SR_SHOP_ROTA.' AS sr'.
                                    ' LEFT JOIN '.SR_SHOP_VOLUNTEERS.' AS ksv ON sr.keyholder = ksv.id'.
                                    ' LEFT JOIN '.SR_SHOP_VOLUNTEERS.' AS nksv ON sr.nonkeyholder = nksv.id'.
                                ' WHERE sr.date > \''.$attributes['range_from'].'\' AND sr.date < \''.$attributes['range_to'].'\''.
                                ' ORDER BY sr.date';
	$results=$wpdb->get_results($query_sql);

    if ($attributes['show_title']) {
        if ($attributes['show_year']) {
            $unixfrom=strtotime($attributes['range_from']);
            $unixto=strtotime($attributes['range_to']);
            $from=date('Y', $unixfrom);
            $to=date('Y', $unixto);
            $range = " $from/$to";
        } else if ($attributes['show_brev_mdate']) {
            $unixfrom=strtotime($attributes['range_from']);
            $unixto=strtotime($attributes['range_to']);
            $from=date('M Y', $unixfrom);
            $to=date('M Y', $unixto);
            $range = " $from to $to";
        } else {
            $unixfrom=strtotime($attributes['range_from']);
            $unixto=strtotime($attributes['range_to']);
            $from=date('Y/m/d', $unixfrom);
            $to=date('Y/m/d', $unixto);
            $range = " $from to $to";
        }
        $out.="<h2>Shop Rota$range</h2>";
    }
	//error_log(__FILE__.':'.__LINE__.' '.var_export($current_user, true));
	//$out.='<h2>User '.$current_user->ID.' - '.$current_user->user_email.' @ '.get_the_author_meta( 'contactnum', $current_user->ID ).' - '.$current_user->display_name.'('.get_the_author_meta('actualname', $current_user->ID).') </h2>';
	//$out.='Attributes: '.var_export($attributes, true)."<br>\n";
    //$out.='_POST: '.var_export($_POST, true)."<br>\n";
    //$out.=$query_sql;
    if (!empty($error)) {
        $out.="<p>Error: $error</p>\n";
    }
    if ($current_id !== NULL) {
        $nonce = wp_create_nonce( 'shop-rota-block-nonce' );
        //$out.="<form action=\"".esc_url($_SERVER[''])."\" method=\"post\">\n";
        //$out.="<form action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
        $out.="<form action=\"\" method=\"post\">\n";
        $out.="<input type=hidden name=shop-rota-block-nonce value=\"${nonce}\">\n";
    }
    $out.="<table border=0 cellspacing=0 cellpadding=0><tbody>";
    $out.="<tr>\n";
    $out.="<th>Date</th>\n";
    $out.="<th>Keyholder</th>\n";
    $out.="<th>NonKeyholder</th>\n";
    if (!empty($current_id)) {
        $out.="<th>Keyholder Claim</th>\n";
        $out.="<th>NonKeyholder Claim</th>\n";
    }
    $out.="</tr>\n";

	foreach($results AS $row) {
        $out.="<tr>\n";
        $out.="<td>".$row->date."</td>\n";
        $out.="<td>".$row->ksv_name."</td>\n";
        $out.="<td>".$row->nksv_name."</td>\n";
        if (!empty($current_id)) {
            $out.="<td>";
            if ($current_keyholder == 1) {
                if ($current_id==$row->ksv_id) {
                    if (!$row->releasekeyholder) {
                        $out.="<button type=\"submit\" name=\"ReleaseKey\" class=\"button\" value=\"$row->id\">Release</button>";
                    } else {
                        $out.="<button type=\"submit\" name=\"ReconfirmKey\" class=\"button\" value=\"$row->id\">Reconfirm</button>";
                    }
                } else {
                    $ksv_disabled=($row->nksv_id==$current_id);
                    if ($row->releasekeyholder && !$ksv_disabled) {
                        $disabled=($ksv_disabled)?" disabled":"";
                        $out.="<button type=\"submit\" name=\"AdoptKey\" class=\"button\" value=\"$row->id\"$disabled>Adopt</button>";
                    }
                }
            }
            $out.="</td>\n";
            $out.="<td>";
            if ($current_id==$row->nksv_id) {
                if (!$row->releasenonkeyholder) {
                    $out.="<button type=\"submit\" name=\"ReleaseNKey\" class=\"button\" value=\"$row->id\">Release</button>";
                } else {
                    $out.="<button type=\"submit\" name=\"ReconfirmNKey\" class=\"button\" value=\"$row->id\">Reconfirm</button>";
                }
            } else {
                $nksv_disabled=($row->ksv_id==$current_id);
                if ($row->releasenonkeyholder && !$nksv_disabled) {
                    $disabled=($nksv_disabled)?" disabled":"";
                    $out.="<button type=\"submit\" name=\"AdoptNKey\" class=\"button\" value=\"$row->id\"$disabled>Adopt</button>";
                }
            }
            $out.="</td>\n";
        }
        $out.="</tr>\n";
    }
    $out.="</tbody></table>";
    if ($current_id !== NULL) {
        $out.="</form>\n";
    }
	//echo esc_attr( get_the_author_meta( 'contactnum', $user->ID ) );
	//display_name
	return $out;

}
