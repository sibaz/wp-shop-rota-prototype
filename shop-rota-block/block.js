( function( blocks, i18n, element ) {
    var el = wp.element.createElement,
        registerBlockType = wp.blocks.registerBlockType,
        ServerSideRender = wp.components.ServerSideRender,
        TextControl = wp.components.TextControl,
        RadioControl = wp.components.RadioControl,
        CheckboxControl = wp.components.CheckboxControl,
        InspectorControls = wp.editor.InspectorControls;
    var __ = i18n.__;

    var blockStyle = {
        backgroundColor: '#900',
        color: '#fff',
        padding: '20px',
    };

  blocks.registerBlockType( 'shop-rota/shop-rota-block', {
    title: __('Shop Rota Sessions'),
    icon: 'universal-access-alt',
    category: 'widgets',
    attributes: {
            range_from : {
                type : 'string'
            },
            range_to : {
                type : 'string'
            },
        },
        example: {
            attributes: {
                range_from: 'then',
                range_to: 'now',
            },
        },
    /*
     * In most other blocks, you'd see an 'attributes' property being defined here.
     * We've defined attributes in the PHP, that information is automatically sent
     * to the block editor, so we don't need to redefine it here.
     */

    edit: function( props ) {
        return [
            /*
             * The ServerSideRender element uses the REST API to automatically call
             * php_block_render() in your PHP code whenever it needs to get an updated
             * view of the block.
             */
            el( ServerSideRender, {
                block: 'shop-rota/shop-rota-block',
                attributes: props.attributes,
            } ),
/*
            el( InspectorControls, {},
                el( wp.components.DateTime, {
                    label: __('Range From'),
                    value: props.attributes.range_from || '',
                    onChange: ( value ) => { props.setAttributes( { range_from: value } ); },
                } )
            ),
            el(
            el( InspectorControls, {},
                wp.components.DatePicker,
                {
                    currentTime : moment(),
                    locale : 'en',
                    //onChange : datecreatedPickerOnChange,
                    //onChange: ( value ) => { props.setAttributes( { range_from: value } ); },
                    //selected : props.attributes.datecreatedPicker
                }
            ),
*/
            el( InspectorControls, {},
/*
			   el( TextControl, {
					label: __('Date From'),
					value: props.attributes.range_from,
					onChange: ( value ) => { console.log(value); props.setAttributes( { range_from: value } ); },
				} ),
*/
                el( wp.components.PanelBody, 
                    {
                        title: __('Date From'),
                    },
                    el( wp.components.DatePicker,
                        {
                            currentTime : '',
                            currentDate : props.attributes.range_from,
                            locale : 'en',
                            //onChange: ( value ) => { console.log(value); props.setAttributes( { range_from: value } ); },
                            onChange: ( value ) => { props.setAttributes( { range_from: value } ); },
				        } ),
                ),
                el( wp.components.PanelBody, 
                    {
                        title: __('Date To'),
                    },
                    el( wp.components.DatePicker,
                        {
                            currentTime : '',
                            currentDate : props.attributes.range_to,
                            locale : 'en',
                            //onChange: ( value ) => { console.log(value); props.setAttributes( { range_from: value } ); },
                            onChange: ( value ) => { props.setAttributes( { range_to: value } ); },
				        } ),
                ),
/*
            el(	'label', {}, __('Date From')),
            el( wp.components.DatePicker,
                {
                    currentTime : '',
                    currentDate : props.attributes.range_from,
                    locale : 'en',
                    //onChange: ( value ) => { console.log(value); props.setAttributes( { range_from: value } ); },
                    onChange: ( value ) => { props.setAttributes( { range_from: value } ); },
				} ),
            el(	'label', {}, __('Date To')),
            el( wp.components.DatePicker,
                {
                    currentTime : '',
                    currentDate : props.attributes.range_to,
                    locale : 'en',
                    //onChange: ( value ) => { console.log(value); props.setAttributes( { range_from: value } ); },
                    onChange: ( value ) => { props.setAttributes( { range_to: value } ); },
				} ),
*/
			),
        ];
    },

    // We're going to be rendering in PHP, so save() can just return null.
    save: function( props ) {
        return [
            /*
             * The ServerSideRender element uses the REST API to automatically call
             * php_block_render() in your PHP code whenever it needs to get an updated
             * view of the block.
             */
            el( ServerSideRender, {
                block: 'shop-rota/shop-rota-block',
                attributes: props.attributes,
            } )

        ];
    },
  } );
}(
    window.wp.blocks,
    window.wp.i18n,
    window.wp.element,
    window.wp.editor
) );

