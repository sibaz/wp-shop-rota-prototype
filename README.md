# Shop Rota Wordpress plugin

This is a plugin intended to manage the East Ward Allotment Association Shop Rota.  

## Contents

### Admin page
- Import / Export volunteers and dates via json
- Present / edit volunteers list and/or shop dates

### Shop Rota Block
- Present a range of shop dates, and allow linked users to release/claim dates

### Cron Job
- Notify forthcoming members of their spot in the rota Month/Fortnight/Week/day before date

# Downloading

The primary store of this project is https://gitlab.com/sibaz/wp-shop-rota-prototype

Release binaries can be downloaded from the [Releases page](https://gitlab.com/sibaz/wp-shop-rota-prototype/-/releases)

Last release was:-
```
wget https://gitlab.com/sibaz/wp-shop-rota-prototype/-/archive/0.1.0/wp-shop-rota-prototype-0.1.0.tar.gz
```
