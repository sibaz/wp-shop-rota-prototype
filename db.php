<?php

global $shop_rota_db_version;
$shop_rota_db_version = '1.0';

global $wpdb;
define('SR_SHOP_ROTA',$wpdb->prefix.'shop_rota');
define('SR_SHOP_VOLUNTEERS',$wpdb->prefix.'shop_rota_volunteers');

function shop_rota_db_install () {
    global $wpdb;
    global $shop_rota_db_version;

    $charset_collate = $wpdb->get_charset_collate();

    $shop_rota_table_name = $wpdb->prefix . "shop_rota";

    $create_shop_rota_table_sql =
    "CREATE TABLE $shop_rota_table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        date date DEFAULT '0000-00-00' NOT NULL,
        keyholder mediumint(9) NOT NULL,
        nonkeyholder mediumint(9) NOT NULL,
        releasekeyholder boolean DEFAULT FALSE NOT NULL,
        releasenonkeyholder boolean DEFAULT FALSE NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";

    $shop_volunteers_table_name = $wpdb->prefix . "shop_rota_volunteers";

    //    lastconsent datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    $create_shop_volunteers_table_sql =
    "CREATE TABLE $shop_volunteers_table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        uid mediumint(9) NOT NULL,
        name tinytext NOT NULL,
        email text DEFAULT ''NOT NULL,
        phone text DEFAULT '' NOT NULL,
        lastconsent datetime,
        keyholder boolean DEFAULT FALSE NOT NULL,
        keycode tinytext DEFAULT '' NOT NULL,
        KEY wp_shop_rota_volunteers_uid (uid),
        PRIMARY KEY  (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $create_shop_rota_table_sql );
    dbDelta( $create_shop_volunteers_table_sql );

    add_option( 'shop_rota_db_version', $shop_rota_db_version );
    update_option( 'shop_rota_db_version', $shop_rota_db_version );
}

function shop_rota_update_db_check() {
    global $shop_rota_db_version;
    if ( get_site_option( 'shop_rota_db_version' ) != $shop_rota_db_version ) {
        shop_rota_db_install();
    }
}

